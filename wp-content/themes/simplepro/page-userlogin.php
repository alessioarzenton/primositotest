<?php get_header(); ?>

<div class="container py-5">

  <div class="row align-items-center justify-content-center py-5">

    <div class="col-md-6 col-10 shadow p-4">

      <form class="text-center">
        <p class="display-1"><i class="bi bi-person-circle"></i></p>
        <h1 class="h3 mb-3 fw-normal">Please sign in</h1>

        <div class="form-floating">
          <input type="email" class="form-control" id="floatingInput" placeholder="name@example.com">
          <label for="floatingInput">Email address</label>
        </div>
        <div class="form-floating">
          <input type="password" class="form-control" id="floatingPassword" placeholder="Password">
          <label for="floatingPassword">Password</label>
        </div>

        <div class="checkbox mb-3 text-start">
          <label>
            <input type="checkbox" value="remember-me"> Remember me
          </label>
        </div>
        <button class="w-100 btn btn-lg btn-primary" type="submit">Sign in</button>
      </form>

    </div>

  </div>

</div>

<?php get_footer(); ?>
