<?php get_header(); ?>

<div class="container py-5">

  <div class="row justify-content-between">

    <div class="col-md-6 col-10">
      <div class="card h-100 shadow-sm">
        <img src="<?php the_post_thumbnail_url(); ?>" class="card-img-top" alt="<?php the_title(); ?>">
        <div class="card-body">
          <h5 class="card-title"><?php the_title(); ?></h5>
          <p class="card-text"><?php the_content(esc_html__('Read More...', 'slug-theme'));?></p>
          <a href="<?php the_permalink(); ?>" class="btn btn-primary">Go somewhere</a>
        </div>
      </div>
    </div>

  </div>
</div>

<?php get_footer(); ?>
