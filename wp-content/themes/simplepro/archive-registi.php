<?php /* Custom Loop */

get_header();

$custom_loop = new WP_Query( array(
  'post_type'      => 'registi',
  'posts_per_page'=> -1,
  'orderby' => 'menu_order',
  'order' => 'ASC'
)); ?>

<div class="container py-5">
  <div class="row justify-content-baseline">

    <h1 class="pb-3 mb-5 text-center border-bottom">Tutti i Registi</h1>

    <?php if ($custom_loop->have_posts()) : while($custom_loop->have_posts()) : $custom_loop->the_post(); ?>

      <div class="col-lg-3 col-8 mb-4">
        <div class="card h-100 shadow-sm">
          <div class="w-100" style="background-image: url(<?php the_post_thumbnail_url(); ?>); height: 300px; background-size: cover; background-repeat: no-repeat;"></div>
          <div class="card-body">
            <h5 class="card-title"><?php the_title(); ?></h5>
            <p class="card-text text-truncate"><?php the_excerpt(); ?></p>
            <a href="<?php the_permalink(); ?>" class="btn btn-primary">Esplora</a>
          </div>
          <div class="card-footer text-muted">
            Generi: <?php the_terms( $post->ID, 'generi'); ?>
          </div>
        </div>
      </div>

      <?php wp_reset_postdata(); ?>
    <?php endwhile; endif; ?>

  </div>
</div>

<?php get_footer(); ?>
