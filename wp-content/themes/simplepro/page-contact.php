<?php get_header(); ?>

<div class="container py-5">

  <div class="row align-items-center justify-content-center py-5">

    <?php
        if (empty($_POST)) {
            echo  '<div class="alert mb-5" role="alert">
            <h4 class="alert-heading text-secondary">Action Log</h4>
            <hr>
            </div>';
        } else {
          global $wpdb;

          $table = $wpdb->prefix . 'users';
          $data = array(
              'user_login' => $_POST['username'],
              'user_email' => $_POST['email'],
              'user_pass'    => $_POST['password']
          )
          
          $success = $wpdb->insert( $table, $data );
          if($success){
              echo '<div class="alert alert-success mb-5" role="alert">
              <h4 class="alert-heading">Well done!</h4>
              <p>Aww yeah, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content.</p>
              <hr>
              <p class="mb-0">Whenever you need to, be sure to use margin utilities to keep things nice and tidy.</p>
              </div>' ;
            }
        }
    ?>

    <div class="col-md-6 col-10 shadow p-4">

      <h2 class="mb-3">Compile the form</h2>

      <form action="" method="POST">
        <?php wp_nonce_field(); ?>
        <div class="mb-3">
          <label for="username" class="form-label">Username</label>
          <input name="username" type="text" class="form-control" id="username" required>
        </div>
        <div class="mb-3">
          <label for="exampleInputEmail1" class="form-label">Email address</label>
          <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required>
        </div>
        <div class="mb-3">
          <label for="exampleInputPassword1" class="form-label">Password</label>
          <input name="password" type="password" class="form-control" id="exampleInputPassword1" required>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>

    </div>

  </div>

</div>

<?php get_footer(); ?>
