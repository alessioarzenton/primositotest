<?php get_header(); ?>

<div class="container py-5">

  <div class="row justify-content-center">

  <?php if (have_posts()) :?><?php while(have_posts()) : the_post(); ?>

    <div class="col-8">
      <div class="card h-100 shadow-sm">
        <img src="<?php the_post_thumbnail_url(); ?>" class="card-img-top" alt="<?php the_title(); ?>">
        <div class="card-body">
          <h5 class="card-title"><?php the_title(); ?></h5>
          <p class="card-text"><?php the_content(esc_html__('Read More...', 'slug-theme'));?></p>
        </div>
        <div class="card-footer text-muted">
          <?php the_time('j M , Y') ?>
        </div>
      </div>
    </div>

  <?php endwhile; ?>
  <?php else : ?>
    <p><?php esc_html_e('Sorry, no posts matched your criteria.', 'slug-theme'); ?></p>
  <?php endif; ?>

  </div>
</div>

<?php get_footer(); ?>
