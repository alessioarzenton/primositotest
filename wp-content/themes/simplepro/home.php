<?php get_header(); ?>

<div class="container py-5">

  <div class="row justify-content-between">

  <?php if (have_posts()) :?><?php while(have_posts()) : the_post(); ?>

    <div class="col">
      <div class="card h-100 shadow-sm">
        <img src="<?php the_post_thumbnail_url(); ?>" class="card-img-top" alt="<?php the_title(); ?>">
        <div class="card-body">
          <h5 class="card-title"><?php the_title(); ?></h5>
          <p class="card-text"><?php the_excerpt();?></p>
          <a href="<?php the_permalink(); ?>" class="btn btn-primary">Go somewhere</a>
        </div>
      </div>
    </div>

  <?php endwhile; ?>
  <?php else : ?>
    <p><?php esc_html_e('Sorry, no posts matched your criteria.', 'slug-theme'); ?></p>
  <?php endif; ?>

  </div>
</div>

<?php get_footer(); ?>
