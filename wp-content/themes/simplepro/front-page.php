<?php get_header(); ?>

<div class="container py-5">

  <div class="row align-items-md-stretch">
      <div class="col-md-6">
        <div class="h-100 p-5 text-white bg-dark rounded-3">
          <h2><?php the_field('home_blog_title'); ?></h2>
          <p><?php the_field('home_blog_description'); ?></p>
          <a href="<?php the_permalink(2); ?>" class="btn btn-outline-light" type="button"><?php the_field('home_blog_button_text'); ?></a>
        </div>
      </div>
      <div class="col-md-6">
        <div class="h-100 p-5 bg-light border rounded-3">
          <h2><?php the_field('home_film_title'); ?></h2>
          <p><?php the_field('home_film_description'); ?></p>
          <a href="<?php echo get_post_type_archive_link('film'); ?>" class="btn btn-outline-secondary" type="button"><?php the_field('home_film_button_text'); ?></a>
        </div>
      </div>
    </div>

</div>

<?php get_footer(); ?>
