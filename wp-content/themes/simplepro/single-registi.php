<?php get_header(); ?>

<div class="container py-5">

  <div class="row justify-content-center">

    <div class="col-md-6 col-8">
      <div class="card h-100 shadow-sm">
        <img src="<?php the_post_thumbnail_url()?>" class="card-img-top img-res">
        <div class="card-body">
          <h5 class="card-title"><?php the_title(); ?></h5>
          <p class="card-text"><?php the_content(); ?></p>
        </div>
        <div class="card-footer text-muted">
          Generi: <?php the_terms( $post->ID, 'generi'); ?>
        </div>
      </div>
    </div>

  </div>
</div>

<?php get_footer(); ?>
