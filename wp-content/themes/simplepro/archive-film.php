<?php /* Custom Loop */

get_header();

$custom_loop = new WP_Query( array(
  'post_type'      => 'film',
  'posts_per_page'=> -1,
  'orderby' => 'menu_order',
  'order' => 'ASC'
)); ?>

<div class="container py-5">
  <div class="row justify-content-baseline">

    <h1 class="pb-3 mb-5 text-center border-bottom">Tutti i Film</h1>

<?php if ($custom_loop->have_posts()) : while($custom_loop->have_posts()) : $custom_loop->the_post(); ?>

    <div class="col-md-4 col-8 mb-4">
      <div class="card h-100 shadow-sm">
        <?php

        $images = get_field('film_gallery');

        if( $images ): ?>
        <div id="carouselFilm" class="carousel slide" data-bs-ride="carousel">
          <div class="carousel-inner">

            <div class="carousel-item active">
              <img src="<?php echo $images[0]['url']; ?>" class="d-block w-100 img-fluid" alt="<?php echo $images[0]['alt']; ?>">
            </div>

            <?php for ($i=1; $i < count($images) ; $i++) { ?>
              <div class="carousel-item">
                <img src="<?php echo $images[$i]['url']; ?>" class="d-block w-100 img-fluid" alt="<?php echo $images[$i]['alt']; ?>">
              </div>
            <?php } ?>

          </div>
          <?php endif; ?>
        </div>
        <div class="card-body">
          <h5 class="card-title"><?php the_title(); ?></h5>
          <p class="card-text text-truncate"><?php the_field('film_description'); ?></p>
          <a href="<?php the_permalink(); ?>" class="btn btn-primary">Esplora</a>
        </div>
        <div class="card-footer text-muted">
          Generi: <?php the_terms( $post->ID, 'generi'); ?> <br>
          Attori: <?php the_terms( $post->ID, 'attori'); ?> <br>
          <?php
          $registi = get_field('registi');
          if( $registi ): ?>
              <?php foreach( $registi as $regista ):
                  $permalink = get_permalink( $regista->ID );
                  ?>
                    Regista: <a href="<?php echo $permalink; ?>"><?php echo $regista->post_title; ?></a>
              <?php endforeach; ?>
          <?php endif; ?>
        </div>
      </div>
    </div>

  <?php wp_reset_postdata(); ?>
<?php endwhile; endif; ?>

  </div>
</div>

<?php get_footer(); ?>
