<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php wp_head(); ?>
  </head>
  <body>

  <header class="p-3 bg-dark text-white">
    <div class="container">
      <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
        <a href="/" class="d-flex align-items-center mb-2 mb-lg-0 text-white text-decoration-none">
          <i class="bi bi-bootstrap-fill"></i>
        </a>

        <?php
        wp_nav_menu(array(
          'theme_location' => 'header',
          'container' => false,
          'items_wrap' => '<ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">%3$s</ul>'
        ));
        ?>

        <form class="col-12 col-lg-auto mb-3 mb-lg-0 me-lg-3">
          <input type="search" class="form-control form-control-dark" placeholder="Search..." aria-label="Search">
        </form>

        <div class="text-end">
          <a href="<?php the_permalink(25); ?>" type="button" class="btn btn-outline-light me-2">Login</a>
          <button type="button" class="btn btn-warning">Sign-up</button>
        </div>
      </div>
    </div>
  </header>

  <?php if (is_home()) { ?>
    <div class="container-fluid" style="width:100vw;height: 60vh;background: linear-gradient(rgba(0,0,0,0.4), rgba(0,0,0,0.8)) ,url('https://picsum.photos/1920/1080')">
      <div class="row justify-content-center align-items-center h-100">

        <h1 class="text-center text-white">Tutti gli articoli</h1>

      </div>
    </div>
  <?php } ?>

  <?php if (is_front_page()) { ?>

    <div class="container-fluid" style="width:100vw;height: 60vh;background-size: cover; background: linear-gradient(rgba(0,0,0,0.4), rgba(0,0,0,0.8)) ,url(<?php the_field('home_image_header'); ?>)">
      <div class="row justify-content-center align-items-center h-100">

        <div class="col-md-5 col-10">
          <h1 class="text-center text-white"><?php bloginfo('name'); ?></h1>
          <h3 class="text-center text-success"><?php the_field('home_subtitle') ?></h3>
        </div>

      </div>
    </div>

  <?php } ?>

  <?php if (is_page() && !is_front_page() && !is_page(25) && !is_single()) { ?>

    <div class="container-fluid" style="width:100vw;height: 60vh;background: linear-gradient(rgba(0,0,0,0.4), rgba(0,0,0,0.8)) ,url('https://picsum.photos/1920/1080')">
      <div class="row justify-content-center align-items-center h-100">

        <h1 class="text-center text-white"><?php the_title(); ?></h1>

      </div>
    </div>

  <?php } ?>
