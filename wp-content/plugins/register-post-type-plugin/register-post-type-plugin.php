<?php
/*
Plugin Name: Register Post Type Plugin
Plugin URI:
Description: Plugin to register custom post type
Version: 1.0
Author: Alessio Arzenton
Author URI:
Textdomain: post-type-plugin
License: GPLv2
*/


//plugin translation

function post_type_plugin_setup() {

    load_plugin_textdomain('post-type-plugin', false, dirname(plugin_basename(__FILE__)) . '/lang/');

} // end custom_theme_setup
add_action('after_setup_theme', 'post_type_plugin_setup');


/* ————————————————————————- *

*   CUSTOM POST TYPE Film

/* ————————————————————————- */

function create_film() {

    $labels = array(
    'name'               => __('Film' , 'post-type-plugin'),
    'singular_name'      => __('Film' , 'post-type-plugin'),
    'add_new'            => __('Aggiungi Film', 'post-type-plugin'),
    'add_new_item'       => __('Aggiungi nuovo Film' , 'post-type-plugin'),
    'edit_item'          => __('Modifica Film', 'post-type-plugin'),
    'new_item'           => __('Nuovo Film', 'post-type-plugin'),
    'all_items'          => __('Tutti i Film', 'post-type-plugin'),
    'view_item'          => __('Visualizza Film' , 'post-type-plugin'),
    'view_items'          => __('Visualizza i Film' , 'post-type-plugin'),
    'search_items'       => __('Cerca Film' , 'post-type-plugin'),
    'not_found'          => __('Film non trovato', 'post-type-plugin'),
    'not_found_in_trash' => __('Film non trovato nel cestino', 'post-type-plugin'),
    );

    $args = array(
    'labels'             => $labels,
    'public'             => true,
    'rewrite'            => array('slug' => 'film'),
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 22,
    'menu_icon' 		 => 'dashicons-format-video',
    'supports'           => array(
    'title',
    'editor',
    'thumbnail',
    'excerpt',
    'page-attributes'
    ),

    );

   register_post_type('film', $args);

}

add_action('init', 'create_film');


/* ————————————————————————- *

*   CUSTOM POST TYPE Registi

/* ————————————————————————- */

function create_registi() {

    $labels = array(
    'name'               => __('Registi' , 'post-type-plugin'),
    'singular_name'      => __('Registi' , 'post-type-plugin'),
    'add_new'            => __('Aggiungi Regista', 'post-type-plugin'),
    'add_new_item'       => __('Aggiungi nuovo Regista' , 'post-type-plugin'),
    'edit_item'          => __('Modifica Regista', 'post-type-plugin'),
    'new_item'           => __('Nuovo Regista', 'post-type-plugin'),
    'all_items'          => __('Tutti i Registi', 'post-type-plugin'),
    'view_item'          => __('Visualizza Regista' , 'post-type-plugin'),
    'view_items'          => __('Visualizza i Registi' , 'post-type-plugin'),
    'search_items'       => __('Cerca Regista' , 'post-type-plugin'),
    'not_found'          => __('Regista non trovato', 'post-type-plugin'),
    'not_found_in_trash' => __('Regista non trovato nel cestino', 'post-type-plugin'),
    );

    $args = array(
    'labels'             => $labels,
    'public'             => true,
    'rewrite'            => array('slug' => 'registi'),
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 22,
    'menu_icon' 		 => 'dashicons-image-filter',
    'supports'           => array(
    'title',
    'editor',
    'thumbnail',
    'excerpt',
    'page-attributes'
    ),

    );

   register_post_type('registi', $args);

}

add_action('init', 'create_registi');
