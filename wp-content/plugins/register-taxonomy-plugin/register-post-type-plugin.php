<?php
/*
Plugin Name: Register Taxonomy Plugin
Plugin URI:
Description: Plugin to register custom taxonomy
Version: 1.0
Author: Alessio Arzenton
Author URI:
Textdomain: taxonomy-plugin
License: GPLv2
*/


//plugin translation

function taxonomy_plugin_setup() {

    load_plugin_textdomain('taxonomy-plugin', false, dirname(plugin_basename(__FILE__)) . '/lang/');

} // end custom_theme_setup
add_action('after_setup_theme', 'taxonomy_plugin_setup');


/* ————————————————————————- *

*   CUSTOM TAXONOMY Generi

/* ————————————————————————- */

function create_generi() {

  $labels = array(
    'name' => _x( 'Generi', 'generi' ),
    'singular_name' => _x( 'Genere', 'genere' ),
    'search_items' =>  __( 'Cerca generi' ),
    'popular_items' => __( 'Generi popolari' ),
    'all_items' => __( 'Tutti i generi' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Modifica genere' ),
    'update_item' => __( 'Aggiorna genere' ),
    'add_new_item' => __( 'Aggiungi nuovo genere' ),
    'new_item_name' => __( 'Nuovo nome genere' ),
    'separate_items_with_commas' => __( 'Separa generi con virgola' ),
    'add_or_remove_items' => __( 'Aggiungi o rimuovi generi' ),
    'choose_from_most_used' => __( 'Scegli dai generi più utilizzati' ),
    'menu_name' => __( 'Generi' ),
  );

  $args = array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    'rewrite' => array( 'slug' => 'genere' ),
  );

	register_taxonomy('generi',['film','registi'],$args);

}
add_action( 'init', 'create_generi' );


/* ————————————————————————- *

*   CUSTOM TAXONOMY Attori

/* ————————————————————————- */

function create_attori() {

  $labels = array(
    'name' => _x( 'Attori', 'attori' ),
    'singular_name' => _x( 'Attore', 'attore' ),
    'search_items' =>  __( 'Cerca attori' ),
    'popular_items' => __( 'Attori popolari' ),
    'all_items' => __( 'Tutti gli attori' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Modifica attore' ),
    'update_item' => __( 'Aggiorna attore' ),
    'add_new_item' => __( 'Aggiungi nuovo attore' ),
    'new_item_name' => __( 'Nuovo nome attore' ),
    'separate_items_with_commas' => __( 'Separa attori con virgola' ),
    'add_or_remove_items' => __( 'Aggiungi o rimuovi attori' ),
    'choose_from_most_used' => __( 'Scegli dagli attori più utilizzati' ),
    'menu_name' => __( 'Attori' ),
  );

  $args = array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    'rewrite' => array( 'slug' => 'attore' ),
  );

	register_taxonomy('attori','film',$args);

}
add_action( 'init', 'create_attori' );
